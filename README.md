
# react-native-my-native-module

## Getting started

`$ npm install react-native-my-native-module --save`

### Mostly automatic installation

`$ react-native link react-native-my-native-module`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-my-native-module` and add `RNMyNativeModule.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNMyNativeModule.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Open info.plist, in the project navigator, add `<key>NSLocationWhenInUseUsageDescription</key>
	<string></string>`
4. Run your project (`Cmd+R`)<

#### Android
1. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-my-native-module'
  	project(':react-native-my-native-module').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-my-native-module/android')
  	```
2. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-my-native-module')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile 'com.google.android.gms:play-services-location:x.y.z'
  	```
- with x.y.z is version you want to add, suggest version higher 11.0.0
4. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNMyNativeModulePackage;` to the imports at the top of the file
  - Add `new RNMyNativeModulePackage()` to the list returned by the `getPackages()` method
5. Add permision for location follow open `android/app/src/main/AndroidManifest.xml`
  - Add `<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />`
  - Add `<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />`






## Usage
```javascript
import RNMyNativeModule from 'react-native-my-native-module';

// TODO: What to do with the module?
RNMyNativeModule;
```
```
const MyModule = new NativeEventEmitter(RNMyNativeModule);
	....
  regiterEvent() {
   Plaftform.OS ==='ios' && MyModule.addListener('authStatusDidChange', status => {
      console.log('=======> status', status);
      alert('status auth location thay doi nay!', status);
    });

    MyModule.addListener('didUpdateLocation', location => {
      this.setState({
        newLocation: location,
      });
    });
  }
```
```
RNMyNativeModule.getTimeStamp()
      .then(timestamp => {
        this.setState({ timestamp });
      })
      .catch(e => alert('Can not get timestapm'));
```

### Medthod
1. helloworld
	- type: callback
	-param: callback(string)
2. getTimeStamp
	-type: promise
	-resolve(Number) -- current timestamp
	-reject(Object) -- Error
3. checkPermissions
	-type: callback
	-param: callback(string)
		+ios: 'authorizedAlways','authorizedWhenInUse','denied','notDetermined','restricted'
		+android: 'granted', 'denied'
4. checkEnableLocationService
	-type: callback
	-param: callback(boolen)
#ios
1. requestWhenInUseAuth
1. requestAlwaysAuth
#android
1. openLocationServiceActivity
	-note: when locationservice (GPS) turn off open setting!
2. startListenLoctionChange
	- note: started listen location change
3. stopListenLocationChange
	-type: promise
	-resolve(true) -- turn off listener loction change
	-reject(false) -- Error

### Event emiter
 1. didUpdateLocation
	- note: emit when loction changed
	- result: object

#ios
 1. authStatusDidChange
	- note: emit when user change setting loction service
	- result: string ('authorizedAlways','authorizedWhenInUse','denied','notDetermined','restricted')


