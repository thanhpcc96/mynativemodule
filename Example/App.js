/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  NativeEventEmitter,
} from 'react-native';
import RNMyNativeModule from 'react-native-my-native-module';

type Props = {};

const MyModule = new NativeEventEmitter(RNMyNativeModule);
export default class App extends Component<Props> {
  constructor(props) {
    super(props);
    this.regiterEvent();
    // RNMyNativeModule.initLocation();
  }
  state = {
    hello: '',
    timestamp: 0,
    newLocation: null,
  };
  componentDidMount() {
    RNMyNativeModule.helloworld(stringHello => {
      this.setState({ hello: stringHello });
    });
    RNMyNativeModule.requestWhenInUseAuth();
  }
  getTimestamp = e => {
    RNMyNativeModule.getTimeStamp()
      .then(timestamp => {
        this.setState({ timestamp });
      })
      .catch(e => alert('Can not get timestapm'));
  };

  /**
   * register event
   */
  regiterEvent() {
    MyModule.addListener('authStatusDidChange', status => {
      console.log('=======> status', status);
      alert('status auth location thay doi nay!', status);
    });
    MyModule.addListener('didUpdateLocation', location => {
      this.setState({
        newLocation: location,
      });
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Text callback tu helloworld()</Text>
        <Text style={{ fontWeight: '600', fontSize: 18, marginVertical: 10 }}>
          {this.state.hello}
        </Text>
        <Text>timestamp exec promise getTimeStamp()</Text>
        <Button
          title="Get timestamp native"
          color="#00DEC0"
          onPress={this.getTimestamp}
        />
        <Text style={{ fontWeight: '600', fontSize: 18, marginVertical: 10 }}>
          {this.state.timestamp}
        </Text>

        <Text>Event emiter khi location thay doi</Text>
        <Text style={{ fontWeight: '600', fontSize: 18, marginVertical: 10 }}>
          {this.state.newLocation
            ? ` lat: ${this.state.newLocation.coords.latitude}, long: ${this.state.newLocation.coords.longitude},
              `
            : undefined}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
