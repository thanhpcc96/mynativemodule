package com.reactlibrary;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
//import android.os.Build.VERSION;
//import java.security.Timestamp;
import java.text.SimpleDateFormat;
import java.sql.Timestamp;
import java.util.Date;
//import com.mohan.location.locationtrack.LocationProvider;

import okio.Timeout;

public class RNMyNativeModuleModule extends ReactContextBaseJavaModule implements LifecycleEventListener {

  private final ReactApplicationContext reactContext;
  private LocationProvider locationProvider;
  private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
  private FusedLocationProviderClient mFusedLocationClient;
  private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

  private LocationCallback mLocationCallback;

  private LocationSettingsRequest mLocationSettingsRequest;
  private LocationRequest mLocationRequest;

  private SettingsClient mSettingsClient;
  // constane for export to JS


  public RNMyNativeModuleModule(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
    mFusedLocationClient = LocationServices.getFusedLocationProviderClient(reactContext);
//    startLocationUpdates();
    createLocationCallback();
    createLocationRequest();
    buildLocationSettingsRequest();
    mSettingsClient = LocationServices.getSettingsClient(this.reactContext);
    getReactApplicationContext().addLifecycleEventListener(this);
  }

  @Override
  public String getName() {
    return "RNMyNativeModule";
  }


  @Override
  public void onHostResume() {

  }

  @Override
  public void onHostPause() {

  }

  @Override
  public void onHostDestroy() {

  }

  /*
  *   Test callback
  *
  * */
  @ReactMethod
  public void helloworld(Callback cb) {
    cb.invoke("Hello world, this messeage sender from native android");
  }

  /*
  *   test promise
  * */
  public void getTimeStamp(Promise promise) {
    try {
      Timestamp timestamp = new Timestamp(System.currentTimeMillis());
      long timestamp2 = timestamp.getTime();
      promise.resolve(timestamp2);
    } catch (Exception e) {
      promise.reject(e);
    }
  }

  /**
   *
   * Location services
   */
  @ReactMethod
  public void checkEnableLocationService(Callback cb) {

    LocationManager manager = (LocationManager) reactContext.getSystemService(Context.LOCATION_SERVICE);
    if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
      cb.invoke(true);
      return;
    } else {
      cb.invoke(false);
      return;
    }
  }


  @ReactMethod
  public void openLocationServiceActivity() {
    final LocationManager manager = (LocationManager) reactContext.getSystemService(Context.LOCATION_SERVICE);
    if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
      reactContext.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
    }
  }


  private void emitMessageToRN(String eventName, @Nullable WritableMap params) {
    this.reactContext
            .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
            .emit(eventName, params);
  }


  @ReactMethod
  public void checkPermissions(Callback cb) {
    int permissionState = ActivityCompat.checkSelfPermission(this.reactContext,
            Manifest.permission.ACCESS_COARSE_LOCATION);
    switch (permissionState) {
      case PackageManager.PERMISSION_GRANTED:
        cb.invoke("granted");
        break;
      case PackageManager.PERMISSION_DENIED:
        cb.invoke("denied");
        break;
    }
  }

  @ReactMethod
  public void startListenLoctionChange() {
    mSettingsClient.checkLocationSettings(mLocationSettingsRequest).addOnSuccessListener(this.reactContext.getCurrentActivity(), new OnSuccessListener<LocationSettingsResponse>() {
      @Override
      public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
        if (ActivityCompat.checkSelfPermission(reactContext.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(reactContext.getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
          // TODO: Consider calling
          //    ActivityCompat#requestPermissions
          // here to request the missing permissions, and then overriding
          //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
          //                                          int[] grantResults)
          // to handle the case where the user grants the permission. See the documentation
          // for ActivityCompat#requestPermissions for more details.
          return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
      }
    });
  }

  @ReactMethod
  public  void stopListenLocationChange(final Promise promise){
    try{
      mFusedLocationClient.removeLocationUpdates(mLocationCallback)
              .addOnCompleteListener(reactContext.getCurrentActivity(), new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                  promise.resolve(true);
                }
              });
    }catch (Exception e){
      Log.e("LOI", "KO tat dk listender");
      promise.resolve(false);
    }
  }


  // helper
  private void createLocationCallback() {
    mLocationCallback = new LocationCallback() {
      @Override
      public void onLocationResult(LocationResult locationResult) {
        super.onLocationResult(locationResult);
        WritableMap params= Arguments.createMap();
        params.putDouble("latitude", locationResult.getLastLocation().getLatitude());
        params.putDouble("longitude", locationResult.getLastLocation().getLongitude());
        emitMessageToRN("didUpdateLocation",params);
      }
    };
  }

  private void buildLocationSettingsRequest() {
    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
    builder.addLocationRequest(mLocationRequest);
    mLocationSettingsRequest = builder.build();
  }

  private void createLocationRequest() {
    mLocationRequest = new LocationRequest();


    mLocationRequest.setInterval(10000);


    mLocationRequest.setFastestInterval(5000);

    mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
  }
}